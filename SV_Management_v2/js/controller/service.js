var svService = {
  getList: function () {
    return axios({
      url: "https://643a58b8bd3623f1b9b1642a.mockapi.io/sv",
      method: "GET",
    });
  },
  remove: function (id) {
    return axios({
      url: `https://643a58b8bd3623f1b9b1642a.mockapi.io/sv/${id}`,
      method: "DELETE",
    });
  },
  create: function (data) {
    return axios({
      url: `https://643a58b8bd3623f1b9b1642a.mockapi.io`,
      method: "POST",
      data: data,
    });
  },
};
