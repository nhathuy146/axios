// đồng bộ: chạy theo thứ tự từ trên xuống dưới

// Bất đồng bộ: gọi api, setTimeOut
setTimeout(function () {
  console.log("Hello");
}, 3000);
console.log(1);
console.log(2);
console.log(3);
// setTimeOut ~ delay
setTimeout(function () {
  console.log("Hello user");
}, 5000);
// Chạy hết code đồng bộ, rồi chuyển sang code bất đồng bộ
