// Then() khi gọi api thành công

// Catch() khi goi api thất bại

// Lấy dữ liệu từ server và render
const BASE_URL = "https://643a58b8bd3623f1b9b1642a.mockapi.io/sv";
var idSelected = null;
function fetchDSSV() {
  batLoading();
  axios({
    url: "https://643a58b8bd3623f1b9b1642a.mockapi.io/sv",
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      // gọi function renderDSSV trong then() => nếu gọi ngoài then sẽ ko có dữ liệu render
      console.log(res.data);
      renderDSSV(res.data.reverse());
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
fetchDSSV();

// Promise

// pendding (loading), success (then), fail(catch)
// Xóa sv
function xoaSV(id) {
  batLoading();
  axios({
    url: `https://643a58b8bd3623f1b9b1642a.mockapi.io/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res.data);
      fetchDSSV();

      Toastify({
        text: "Xóa sinh viên thành công",
        offset: {
          x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
          y: 10, // vertical axis - can be a number or a string indicating unity. eg: '2em'
        },
      }).showToast();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function themSinhVien() {
  var dataSv = layThongTinTuFrom();
  axios({
    url: BASE_URL,
    method: "POST",
    data: dataSv,
  })
    .then(function (res) {
      tatLoading();
      // Gọi lại api lấy danh sách mới nhất từ server sau khi xóa thành công
      fetchDSSV();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function suaSV(id) {
  idSelected = id;
  batLoading();
  axios({
    url: `https://643a58b8bd3623f1b9b1642a.mockapi.io/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(tatLoading());
}

function capNhatSinhVien(id) {
  batLoading();
  axios({
    url: "BASE_URL" + "/" + "id",
    method: "PUT",
    data: layThongTinTuFrom(),
  })
    .then(function (res) {
      tatLoading();
      fetchDSSV(res.data);
    })
    .catch(tatLoading());
}
